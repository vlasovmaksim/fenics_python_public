__author__ = 'maks'

"""
The module defines additional functions using for solving a problem.
"""

from dolfin import *
import numpy

import additional_functions


def solve_problem(F, function_1, function_0, bcs_, is_newton, iter_,
                  max_iter, tol, eps, trial_function=None,
                  is_transient=False, time=None, time_end=None, delta_t=None,
                  is_plot_solution=False, omega=None, is_iterative_solver=False,
                  linear_solver="default", preconditioner="default"):
    """
    Solve Navier-Stokes stationary problem with Picard/Newton method, Transient/Stationary,
    Slip/NoSlip boundary conditions.

    :rtype : dolfin.functions.function.Function
    :type F: ufl.form.Form
    :type function_1: dolfin.functions.function.Function
    :type function_0: dolfin.functions.function.Function
    :type bcs_: list
    :type is_newton: bool
    :type iter_: int
    :type max_iter: int
    :type tol: float
    :type eps: float
    :type trial_function: dolfin.functions.function.Argument
    :type is_transient: bool
    :type time: float
    :type time_end: float
    :type delta_t: float
    :type is_plot_solution: bool
    :type omega: float
    :type is_iterative_solver: bool
    :type linear_solver: str
    :type preconditioner: str
    :return: the solution
    """

    if not is_newton:
        a, L = prepare_solution_picard(F)
    else:
        F, J = prepare_solution_newton(F, function_0, trial_function)

        a = J
        L = -F

    if is_transient:
        function_0 = solve_transient(a, L, function_1, function_0, bcs_, iter_, max_iter,
                                     tol, eps, is_plot_solution, omega, is_iterative_solver,
                                     linear_solver, preconditioner, time, time_end, delta_t)
    else:
        function_0 = solve_stationary(a, L, function_1, function_0, bcs_, iter_, max_iter,
                                      tol, eps, is_plot_solution, omega, is_iterative_solver,
                                      linear_solver, preconditioner)

    return function_0


def prepare_solution_picard(F):
    """
    Define Bilinear and Linear form for Picard method.

    :rtype : ufl.form.Form
    :type F: ufl.form.Form
    :return: bilinear form, linear form
    """

    a = lhs(F)
    L = rhs(F)

    return a, L


def prepare_solution_newton(F, function_0, trial_function):
    """
    Define Form and Jacobian for Newton method.

    :rtype : ufl.form.Form
    :type F: ufl.form.Form
    :type function_0: dolfin.functions.function.Function
    :type trial_function: dolfin.functions.function.Argument
    :return: form, jacobian
    """

    F = action(F, function_0)
    J = derivative(F, function_0, trial_function)

    return F, J


def solve_stationary(a, L, function_1, function_0, bcs_, iter_, max_iter,
                     tol, eps, is_plot_solution, omega, is_iterative_solver,
                     linear_solver, preconditioner):
    """
    Solve a stationary problem.

    :rtype : dolfin.functions.function.Function
    :type a: ufl.form.Form
    :type L: ufl.form.Form
    :type function_1: dolfin.functions.function.Function
    :type function_0: dolfin.functions.function.Function
    :type bcs_: list
    :type iter_: int
    :type max_iter: int
    :type tol: float
    :type eps: float
    :type is_plot_solution: bool
    :type omega: float
    :type is_iterative_solver: bool
    :type linear_solver: str
    :type preconditioner: str
    :return: the solution
    """

    #remove
    # file_1, file_2 = additional_functions.create_files("results")

    while (iter_ < max_iter and
           eps > tol):

        iter_ += 1

        function_1 = solve_linear_problem(a, L, function_1, bcs_, is_iterative_solver, linear_solver, preconditioner)

        diff_up = function_1.vector().array() - function_0.vector().array()
        eps = numpy.linalg.norm(diff_up, ord=numpy.Inf)

        print "iter = {:d}; eps_up = {:e}\n".format(iter_, eps)

        if (omega == 1.0):
            function_1.vector().axpy(-1, function_0.vector())
            function_0.vector().axpy(omega, function_1.vector())
        else:
            function_0.assign(function_1)

        # Or like this below
        # function_0.assign(function_1)

        additional_functions.plot_solution(function_0, is_plot_solution)

        #remove
        # additional_functions.save_solution(function_1, file_1, file_2, True)

    return function_0


def solve_transient(a, L, function_1, function_0, bcs_, iter_, max_iter,
                    tol, eps, is_plot_solution, omega, is_iterative_solver,
                    linear_solver, preconditioner, time, time_end, delta_t):
    """
    Solve a transient problem.

    :rtype : dolfin.functions.function.Function
    :type a: ufl.form.Form
    :type L: ufl.form.Form
    :type function_1: dolfin.functions.function.Function
    :type function_0: dolfin.functions.function.Function
    :type bcs_: list
    :type iter_: int
    :type max_iter: int
    :type tol: float
    :type eps: float
    :type is_plot_solution: bool
    :type omega: float
    :type is_iterative_solver: bool
    :type linear_solver: str
    :type preconditioner: str
    :type time: float
    :type time_end: float
    :type delta_t: float
    :return: the solution
    """

    while (time < time_end and
           iter_ < max_iter and
           eps > tol):

        iter_ += 1
        time += delta_t

        function_1 = solve_linear_problem(a, L, function_1, bcs_, is_iterative_solver, linear_solver, preconditioner)

        diff_up = function_1.vector().array() - function_0.vector().array()
        eps = numpy.linalg.norm(diff_up, ord=numpy.Inf)

        print "iter = {:d}; eps_up = {:e}; time = {:.2f}\n".format(iter_, eps, time)

        if omega == 1.0:
            function_1.vector().axpy(-1, function_0.vector())
            function_0.vector().axpy(omega, function_1.vector())
        else:
            function_0.assign(function_1)

        # Or like this below
        # function_0.assign(function_1)

        additional_functions.plot_solution(function_0, is_plot_solution)

        # dolfin.info(dolfin.memory_usage(True))

    return function_0


def solve_linear_problem(a, L, function_, bcs_, is_iterative_solver, linear_solver, preconditioner):
    """
    Solve a linear problem.

    :rtype : dolfin.functions.function.Function
    :type a: ufl.form.Form
    :type L: ufl.form.Form
    :type function_: dolfin.functions.function.Function
    :type bcs_: list
    :type is_iterative_solver: bool
    :type linear_solver: str
    :type preconditioner: str
    :return: the solution
    """

    # info(LinearVariationalSolver.default_parameters(), True)

    timer_solver = Timer("TimerSolver")
    timer_solver.start()

    problem = LinearVariationalProblem(a, L, function_, bcs_)
    solver = LinearVariationalSolver(problem)

    if is_iterative_solver:
        solver.parameters["linear_solver"] = linear_solver
        solver.parameters["preconditioner"] = preconditioner
    else:
        solver.parameters["linear_solver"] = linear_solver

    solver.solve()

    timer_solver.stop()
    info("TimerSolver = %g" % timer_solver.value())

    return function_


def solve_nonlinear_problem(F, function_, bcs_, J, is_iterative_solver, linear_solver, preconditioner):
    """
    Solve a problem with the Newton method.

    :rtype : dolfin.functions.function.Function
    :type F: ufl.form.Form
    :type function_: dolfin.functions.function.Function
    :type bcs_: list
    :type J: ufl.form.Form
    :type is_iterative_solver: bool
    :type linear_solver: str
    :type preconditioner: str
    :return: the solution
    """

    # info(NonlinearVariationalSolver.default_parameters(), True)

    timer_solver = Timer("TimerSolver")
    timer_solver.start()

    problem = NonlinearVariationalProblem(F, function_, bcs_, J)
    solver = NonlinearVariationalSolver(problem)

    # solver.parameters["newton_solver"]["relaxation_parameter"] = 0.9

    if is_iterative_solver:
        solver.parameters["newton_solver"]["linear_solver"] = linear_solver
        solver.parameters["newton_solver"]["preconditioner"] = preconditioner

    solver.solve()

    timer_solver.stop()
    info("TimerSolver = %g" % timer_solver.value())

    return function_


def solve_linear(a, L, function_, bcs_, linear_solver, preconditioner):
    """
    Solve a linear problem.

    :rtype : dolfin.functions.function.Function
    :type a: ufl.form.Form
    :type L: ufl.form.Form
    :type function_: dolfin.functions.function.Function
    :type bcs_: list
    :type linear_solver: str
    :type preconditioner: str
    :return: the solution
    """

    timer_solver = Timer("TimerSolver")
    timer_solver.start()

    A = assemble(a)
    b = assemble(L)

    [bc.apply(A, b) for bc in bcs_]

    solve(A, function_.vector(), b, linear_solver, preconditioner)

    timer_solver.stop()
    info("TimerSolver = %g" % timer_solver.value())

    return function_

