__author__ = 'maks'

from dolfin import *

import numpy as np

import additional_functions
import additional_solve_functions


def solve_(parameters_dict=None):
    """
    Solve a problem with Navier-Stokes Coupled solver

    :rtype : None
    :type parameters_dict: dict
    """

    assert parameters_dict

    # Receive a parameters from a dictionary
    is_slip = parameters_dict["is_slip"]
    is_iterative_solver = parameters_dict["is_iterative_solver"]
    f = parameters_dict["f"]
    is_save_solution = parameters_dict["is_save_solution"]
    is_plot_solution = parameters_dict["is_plot_solution"]
    folder_name = parameters_dict["folder_name"]
    is_newton = parameters_dict["is_newton"]
    is_transient = parameters_dict["is_transient"]
    is_convection = parameters_dict["is_convection"]
    is_slip = parameters_dict["is_slip"]
    is_problem_3d = parameters_dict["is_problem_3d"]
    time = parameters_dict["time"]
    time_end = parameters_dict["time_end"]
    delta_t = parameters_dict["delta_t"]

    linear_solver = parameters_dict["linear_solver"]
    preconditioner = parameters_dict["preconditioner"]
    nu = parameters_dict["nu"]
    p_inlet = parameters_dict["p_inlet"]
    p_outlet = parameters_dict["p_outlet"]
    u_inlet = parameters_dict["u_inlet"]
    iter_ = parameters_dict["iter_"]
    max_iter = parameters_dict["max_iter"]
    tol = parameters_dict["tol"]
    eps = parameters_dict["eps"]
    omega = parameters_dict["omega"]

    is_stabilization = parameters_dict["is_stabilization"]
    mesh = parameters_dict["mesh"]
    dimension = parameters_dict["dimension"]

    is_slip_test = parameters_dict["is_slip_test"]



    # Print parameters_dict.keys() in a sorted list
    print "parameters_dict:"
    parameters_keys_list = parameters_dict.keys()
    parameters_keys_list.sort()

    for key in parameters_keys_list:
        print "{:s} = {:s}".format(key, str(parameters_dict[key]))

    print "*" * 25

    # info(mesh)

    n = FacetNormal(mesh)

    cell_size = CellSize(mesh)

    # plot(cell_size, mesh=mesh, interactive=True)

    # plot(mesh)
    # interactive()

    # Define function spaces
    if not is_stabilization:
        V = VectorFunctionSpace(mesh, "CG", 2)
    else:
        V = VectorFunctionSpace(mesh, "CG", 1)

    Q = FunctionSpace(mesh, "CG", 1)
    VQ = V * Q

    # Define functions
    up = TrialFunction(VQ)
    u, p = split(up)

    v_u, v_p = TestFunctions(VQ)

    up_1 = Function(VQ)
    up_0 = Function(VQ)
    u_0, p_0 = split(up_0)

    #region Interpolate an initial values
    if dimension == 3:
        up_expr = Constant((0.0, 0.0, 1.0, 0.0))
    else:
        up_expr = Constant((1.0, 0.0, 0.0))

    if not is_slip_test or is_transient:
        up_0 = interpolate(up_expr, VQ)
        u_0, p_0 = split(up_0)   
     
    # plot(up_0, interactive=True)
    # plot(u_0, interactive=True)
    #endregion

    if (dimension == 3):
        # Define Sub domain (inflow)
        class Inlet(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and dolfin.near(x[2], 0.0)

        # Define Sub domain (outflow)
        class Outlet(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and dolfin.near(x[2], 10.0)
    elif (dimension == 2):
        # Define Sub domain (inflow)
        class Inlet(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and dolfin.near(x[0], 0.0)

        # Define Sub domain (outflow)
        class Outlet(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and dolfin.near(x[0], 10.0)

        # Define Sub domain (Walls)
        class Walls(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary

    if (dimension == 3) and is_slip and not is_slip_test:
        # Define Sub domain (inflow)
        class Walls_x(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (dolfin.near(x[0], 0.0) or dolfin.near(x[0], 1.0))

        # Define Sub domain (outflow)
        class Walls_y(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (dolfin.near(x[1], 0.0) or dolfin.near(x[1], 1.0))
    elif (dimension == 3) and (not is_slip or is_slip_test):
        # Define Sub domain (Walls)
        class Walls(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary

    # Define mesh boundaries
    domains = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)

    domains.set_all(0)

    inlet = Inlet()
    outlet = Outlet()

    if (dimension == 3) and is_slip and not is_slip_test:
        walls_x = Walls_x()
        walls_y = Walls_y()
    else:
        walls = Walls()

    if (dimension == 3) and is_slip and not is_slip_test:
        walls_x.mark(domains, 3)
        walls_y.mark(domains, 4)
    else:
        walls.mark(domains, 3)

    inlet.mark(domains, 1)
    outlet.mark(domains, 2)


    # plot(domains, interactive=True)

    ds = dolfin.Measure("ds")[domains]

    #region No need.
    # Save sub domains to the file
    # file = File(folder_name + "/domains.pvd")
    # file << domains

    # Define boundary conditions
    # u_inlet_const = Constant((u_inlet, 0.0))
    # u_wall_const = Constant((0.0, 0.0))

    # print "{}".format((u_inlet,) + (0.0,)*(dimension - 1))

    # test = (0,) + (0,)
    # print test
    #endregion

    if dimension == 3:
        u_inlet_const = Constant((0.0, 0.0, u_inlet))
    elif dimension == 2:
        u_inlet_const = Constant((u_inlet, 0.0))

    u_wall_const = Constant((0.0,) * dimension)

    p_1_const = Constant(p_inlet)
    p_2_const = Constant(p_outlet)

    # u_inlet_parabolic = Expression(("1.0", "0.0"))

    if dimension == 3:
        u_inlet_parabolic = Expression(("u_max * (1.0 - pow(((x[1] - R)/R), 2))", "0.0", "0.0"), u_max=0.0, R=0.0)
    else:
        u_inlet_parabolic = Expression(("u_max * (1.0 - pow(((x[1] - R)/R), 2))", "0.0"), u_max=0.0, R=0.0)

    u_inlet_parabolic.u_max = 1.5
    u_inlet_parabolic.R = 0.5

    zero = Constant(0.0)

    # Inlet BC
    bc_u_1 = DirichletBC(VQ.sub(0), u_inlet_const, domains, 1)
    bc_u_1_parabolic = DirichletBC(VQ.sub(0), u_inlet_parabolic, domains, 1)
    # bc_p_1 = DirichletBC(VQ.sub(1), p_1_const, domains, 1)

    # Wall BC
    bc_u_3 = DirichletBC(VQ.sub(0), u_wall_const, domains, 3)

    if (dimension == 3) and is_slip:
        bc_u_3_slip_x = DirichletBC(VQ.sub(0).sub(0), zero, domains, 3)
        bc_u_3_slip_y = DirichletBC(VQ.sub(0).sub(1), zero, domains, 4)
    else:
        bc_u_3_slip = DirichletBC(VQ.sub(0).sub(1), zero, domains, 3)

    # Outlet BC
    bc_p_2 = DirichletBC(VQ.sub(1), p_2_const, domains, 2)

    # is_plot_bc = True
    is_plot_bc = False

    if is_plot_bc:
        bc_func = dolfin.Function(VQ)
        # bc_u_1_parabolic.apply(bc_func.vector())
        bc_u_1.apply(bc_func.vector())

        dolfin.plot(bc_func.split()[0])

        file_bc = dolfin.File("results/bc.pvd")
        file_bc << bc_func.split()[0]

        dolfin.interactive()

    # bcs_up = []

    dt = Constant(delta_t)

    # Define Stokes Coupled equations
    # Stokes NoSlip
    class NSCoupled1(object):
        def __init__(self):
            self.bcs_up = None
            self.F = None
            self.f = f

            if is_stabilization:
                self.betta = 0.2

                # self.betta = 0.02
                # self.betta = 1.0
                # self.betta = 100.0

                self.betta_const = Constant(self.betta)
                # self.delta = self.betta_const * pow(cell_size, 2)

                self.delta = self.betta_const * pow(cell_size, 2) / 4.0 / nu

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1, bc_u_3]

        def define_equation(self):
            self.F = nu * inner(grad(u) + grad(u).T, grad(v_u)) * dx \
                     - p * div(v_u) * dx \
                     + div(u) * v_p * dx \
                     - inner(self.f, v_u) * dx

        def add_stabilization(self):
            self.F += self.delta * \
                      inner((dot(grad(u), u_0)
                             + grad(p)
                             - div(nu * (grad(u) + grad(u).T))
                             - self.f
                            ), grad(v_p)) * dx

        def add_transient_term(self):
            self.F += (1 / dt) * inner(u - u_0, v_u) * dx

        def add_convective_term(self):
            self.F += inner(dot(grad(u), u_0), v_u) * dx

        def initialise(self):
            self.define_boundary_conditions()
            self.define_equation()
            if is_stabilization:
                self.add_stabilization()
            if is_transient:
                self.add_transient_term()
            if is_convection:
                self.add_convective_term()

    class NSCoupledOut1(NSCoupled1):
        def __init__(self):
            super(NSCoupledOut1, self).__init__()

        def define_equation(self):
            super(NSCoupledOut1, self).define_equation()

            self.F += -nu * inner(grad(u) * n + grad(u).T * n, v_u) * ds(2)

    class NSCoupledOut1Parabolic(NSCoupledOut1):
        def __init__(self):
            super(NSCoupledOut1Parabolic, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1_parabolic, bc_u_3]

    class NSCoupledOut2(NSCoupled1):
        def __init__(self):
            super(NSCoupledOut2, self).__init__()

        def define_equation(self):
            super(NSCoupledOut2, self).define_equation()

            self.F += -nu * inner(grad(u).T * n, v_u) * ds(2)

    class NSCoupledOut2Parabolic(NSCoupledOut2):
        def __init__(self):
            super(NSCoupledOut2Parabolic, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1_parabolic, bc_u_3]

    class NSCoupledOut3(NSCoupled1):
        def __init__(self):
            super(NSCoupledOut3, self).__init__()

    class NSCoupledOut3Parabolic(NSCoupledOut3):
        def __init__(self):
            super(NSCoupledOut3Parabolic, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1_parabolic, bc_u_3]

    class NSCoupledOut4(NSCoupled1):
        def __init__(self):
            super(NSCoupledOut4, self).__init__()

        def define_equation(self):
            super(NSCoupledOut4, self).define_equation()

            self.F += -nu * inner(grad(u) * n, v_u) * ds(2)

    class NSCoupledOut4Parabolic(NSCoupledOut4):
        def __init__(self):
            super(NSCoupledOut4Parabolic, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1_parabolic, bc_u_3]

    class NSCoupledOut5(NSCoupledOut2):
        def __init__(self):
            super(NSCoupledOut5, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_3]

        def define_equation(self):
            super(NSCoupledOut5, self).define_equation()

            self.F += inner(p_1_const * n, v_u) * ds(1)

            self.F += inner(p_2_const * n, v_u) * ds(2)

    class NSCoupledOut6(NSCoupled1):
        def __init__(self):
            super(NSCoupledOut6, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1, bc_u_3, bc_p_2]

    class NSCoupledOut7(NSCoupledOut2):
        def __init__(self):
            super(NSCoupledOut7, self).__init__()

        # Doesn't work. Use weak form of p_out bc.
        # def define_boundary_conditions(self):
        #     self.bcs_up = [bc_u_1, bc_u_3, bc_p_2]

        def define_equation(self):
            super(NSCoupledOut7, self).define_equation()

            # Weak form of p_out bc.
            self.F += inner(p_2_const * n, v_u) * ds(2)

    class NSCoupledOut7Parabolic(NSCoupledOut7):
        def __init__(self):
            super(NSCoupledOut7Parabolic, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1_parabolic, bc_u_3]

    # Stokes Slip
    class NSCoupledSlip1(NSCoupled1):
        def __init__(self):
            super(NSCoupledSlip1, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1]

        def define_equation(self):
            super(NSCoupledSlip1, self).define_equation()

            # Base case
            # self.F += - nu*inner(grad(u)*n + grad(u).T*n, v_u)*ds(3) \
            #     + inner(p*n, v_u)*ds(3)

            # grad(u)*n = 0
            self.F += - nu * inner(grad(u).T * n, v_u) * ds(3) \
                      + inner(p * n, v_u) * ds(3)

            # Outlet BC
            # self.F += - nu*inner(grad(u).T*n, v_u)*ds(2)

            # d(u)/d(n) = -u/constant
            # delta_slip = Constant(1000.0)
            # self.F += - nu*inner(-u/delta_slip + grad(u).T*n, v_u)*ds(3) \
            #     + inner(p*n, v_u)*ds(3)

            # d(u)/dn = 0
            # (d(u)/dn).T = 0
            # self.F += inner(p*n, v_u)*ds(3)

    class NSCoupledSlip2(NSCoupledOut2):
        def __init__(self):
            super(NSCoupledSlip2, self).__init__()

        def define_boundary_conditions(self):
            if dimension == 3:
                self.bcs_up = [bc_u_1, bc_u_3_slip_x, bc_u_3_slip_y]
            else:
                self.bcs_up = [bc_u_1, bc_u_3_slip]

    class NSCoupledSlip5(NSCoupledOut5):
        def __init__(self):
            super(NSCoupledSlip5, self).__init__()

        def define_boundary_conditions(self):
            if dimension == 3:
                self.bcs_up = [bc_u_3_slip_x, bc_u_3_slip_y]
            else:
                self.bcs_up = [bc_u_3_slip]

    class NSCoupledSlip7(NSCoupledOut7):
        def __init__(self):
            super(NSCoupledSlip7, self).__init__()

        def define_boundary_conditions(self):
            if dimension == 3:
                self.bcs_up = [bc_u_1, bc_u_3_slip_x, bc_u_3_slip_y]
            else:
                self.bcs_up = [bc_u_1, bc_u_3_slip]

    class NSCoupledSlip6(NSCoupled1):
        def __init__(self):
            super(NSCoupledSlip6, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1]

        def define_equation(self):
            super(NSCoupledSlip6, self).define_equation()

            beta = Constant(10.0)

            # def t(u, p): return dot(2 * nu * sym(grad(u)), n) - p * n
            def t(k1, k2): return dot(2 * nu * sym(grad(k1)), n) - k2 * n

            self.F += - dot(n, t(u, p)) * dot(v_u, n) * ds(3) \
                      - dot(u, n) * dot(n, t(v_u, v_p)) * ds(3) \
                      + beta / cell_size * dot(u, n) * dot(v_u, n) * ds(3)

            # Weak form of p_out bc.
            self.F += inner(p_2_const * n, v_u) * ds(2)

    # Newton NoSlip Steady
    class NSCoupledNewton1(NSCoupled1):
        def __init__(self):
            super(NSCoupledNewton1, self).__init__()

        def add_convective_term(self):
            self.F += inner(dot(grad(u), u), v_u) * dx

        def add_stabilization(self):
            self.F += self.delta * \
                      inner((dot(grad(u), u)
                             + grad(p)
                             - div(nu * (grad(u) + grad(u).T))
                             - self.f
                            ), grad(v_p)) * dx

    class NSCoupledNewtonSlip1(NSCoupledNewton1):
        def __init__(self):
            super(NSCoupledNewtonSlip1, self).__init__()

        def define_boundary_conditions(self):
            self.bcs_up = [bc_u_1]

        def define_equation(self):
            super(NSCoupledNewtonSlip1, self).define_equation()

            # Base case
            # self.F += - nu*inner(grad(u)*n + grad(u).T*n, v_u)*ds(3) \
            #     + inner(p*n, v_u)*ds(3)

            # grad(u)*n = 0
            self.F += - nu * inner(grad(u).T * n, v_u) * ds(3) \
                      + inner(p * n, v_u) * ds(3)

    #region No need
    # Newton NoSlip Transient
    # class NSCoupledTransientNewton1(NSCoupledNewton1):
    #     def __init__(self):
    #         super(NSCoupledTransientNewton1, self).__init__()
    #
    #     def define_equation(self):
    #         super(NSCoupledTransientNewton1, self).define_equation()
    #
    #         self.F += (1/dt) * inner(u - u_0, v_u)*dx

    # # Newton Slip Steady
    # class NSCoupledNewtonSlip1(NSCoupledSlip1):
    #     def __init__(self):
    #         super(NSCoupledNewtonSlip1, self).__init__()
    #
    #     def define_equation(self):
    #         super(NSCoupledNewtonSlip1, self).define_equation()
    #
    #         self.F += inner(dot(grad(u), u), v_u)*dx
    #
    # # Newton Slip Transient
    # class NSCoupledTransientNewtonSlip1(NSCoupledNewtonSlip1):
    #     def __init__(self):
    #         super(NSCoupledTransientNewtonSlip1, self).__init__()
    #
    #     def define_equation(self):
    #         super(NSCoupledTransientNewtonSlip1, self).define_equation()
    #
    #         self.F += (1/dt) * inner(u - u_0, v_u)*dx
    #endregion

    # Use Picard method
    if not is_newton:
        if not is_slip:
            # ns_ = NSCoupled1()

            ns_ = NSCoupledOut1()
            # ns_ = NSCoupledOut2()
            # ns_ = NSCoupledOut3()
            # ns_ = NSCoupledOut4()
            # ns_ = NSCoupledOut5()
            # ns_ = NSCoupledOut6()
            # ns_ = NSCoupledOut7()

            # ns_ = NSCoupledOut1Parabolic()
            # ns_ = NSCoupledOut2Parabolic()
            # ns_ = NSCoupledOut3Parabolic()
            # ns_ = NSCoupledOut4Parabolic()
            # ns_ = NSCoupledOut7Parabolic()
        else:
            # ns_ = NSCoupledSlip1()

            # ns_ = NSCoupledSlip2()
            # ns_ = NSCoupledSlip5()
            # ns_ = NSCoupledSlip7()
            ns_ = NSCoupledSlip6()

    # Use Newton method
    if is_newton:
        if not is_slip:
            ns_ = NSCoupledNewton1()
        else:
            ns_ = NSCoupledNewtonSlip1()

    assert ns_

    # Print current using Navier-Stokes class
    info(str(ns_))
    print "*" * 25

    ns_.initialise()

    F = ns_.F

    assert F

    bcs_up = ns_.bcs_up

    # Create a files for storing a solution
    file_1, file_2 = additional_functions.create_files(folder_name)

    # plot(ns_.f, mesh=mesh)

    timer_solver_all = Timer("TimerSolveAll")
    timer_solver_all.start()

    # Solve a problem
    up_1 = additional_solve_functions.solve_problem(F, up_1, up_0, bcs_up, is_newton, iter_,
                                                    max_iter, tol, eps, up, is_transient, time,
                                                    time_end, delta_t, is_plot_solution, omega,
                                                    is_iterative_solver, linear_solver, preconditioner)

    timer_solver_all.stop()
    info("TimerSolveAll = %g" % timer_solver_all.value())

    #region No need.
    # p_avg = avg(up_1.split()[1])*ds(1)
    # p_avg = inner(p, v_p)*ds(1)
    # p_avg = inner(p, v_p)*ds(1)
    # p_avg = p_0.
    # p_average = assemble(p_avg, mesh=mesh)
    #endregion

    u, p = up_1.split(deepcopy=True)[:]

    #region No need.
    # u = up_1.split()[0]
    # p = up_1.split()[1]

    # u_array = u.vector().array()
    # p_array = p.vector().array()
    # avgU = np.average(u_array)
    # avgP = np.average(p_array)

    # print avgU
    # print avgP

    # dolfin.plot(p_average, title="p_avg", mesh=mesh)
    # print p_average
    #endregion

    additional_functions.plot_solution(up_1, is_plot_solution)

    additional_functions.save_solution(up_1, file_1, file_2, is_save_solution)

    print "*" * 25
    print "end"

    interactive()